const fetch = require("node-fetch");
const connection = require('../../config/db_connection');

exports.getBooks = (res, callback) => {
  const url = "https://scb-test-book-publisher.herokuapp.com/books";
  const getData = async url => {
    try {
      const response = await fetch(url);
      const data = await response.json();
      callback(data);
    } catch (error) {
      return res.status(500).json(err);
    }
  };
  getData(url);
};

exports.getRecommendBook = (res, callback) => {
  const url = "https://scb-test-book-publisher.herokuapp.com/books/recommendation";
  const getData = async url => {
    try {
      const response = await fetch(url);
      const data = await response.json();
      callback(data);
    } catch (error) {
      return res.status(500).send(error);
    }
  }
  getData(url);
};