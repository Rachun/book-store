const connection = require('../../config/db_connection');
const bookModel = require('../models/book');
const Joi = require('joi');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const dateFormat = require('dateformat');


exports.getInfo = (id, callback) => {
  const query = "SELECT * FROM user WHERE id=?";
  connection.query(query, id, (err, rows, fields) => {
    if (err) return err;
    if (rows.length == 0) return callback(rows);

    const row = rows[0];
    const info = {
      id: row.id,
      name: row.name,
      username: row.username,
      birthdate: dateFormat(row.birthdate, "dd/mm/yyyy")
    };
    callback(info);
  });
};

exports.create = (req, res, callback) => {
  const rules = Joi.object().keys({
    name: Joi.string().required(),
    surname: Joi.string().required(),
    username: Joi.string().required(),
    password: Joi.string().required(),
    birthdate: Joi.string().optional()
  });

  //validate input
  const result = Joi.validate(req.body, rules);

  if (result.error) {
    return res.status(400).send(result.error.details[0].message);
  }

  //check username is exists
  const query = "SELECT * FROM user where username=?";
  connection.query(query, req.body.username, (err, rows, fields) => {
    if (err) throw err;
    if (rows.length > 0) return res.status(400).send('The username has been taken!');

    //encrypt password then save
    bcrypt.hash(req.body.password, 10, (err, hash) => {
      if (err) return res.status(500).json(err);

      req.body.password = hash;
      const query = "INSERT INTO user SET ?";

      connection.query(query, req.body, (err, rows, fields) => {
        if (err) return res.status(500).json(err);
        callback(rows.insertId);
      });
    });
  });
};

exports.login = (req, res, callback) => {
  const rules = Joi.object().keys({
    username: Joi.string().required(),
    password: Joi.string().required(),
  });

  //validate input
  const result = Joi.validate(req.body, rules);
  if (result.error) {
    return res.status(400).send(result.error.details[0].message);
  }

  //check user is exists
  const query = "SELECT * FROM user where username=?";
  connection.query(query, req.body.username, (err, rows, fields) => {
    if (err) throw err;
    if (rows.length == 0) return res.status(400).send('Invalid username or password!');

    bcrypt.compare(req.body.password, rows[0].password, (err, response) => {
      if (err) return response.status(500).send(err);
      callback(response, rows[0]);
    });
  });
};

exports.delete = (id, callback) => {
  const query = "Delete FROM user WHERE id=?";
  connection.query(query, id, (err, rows, fields) => {
    if (err) return err;
    const query = "Delete FROM orders WHERE user_id=?";
    connection.query(query, id, (err, rows, fields) => {
      if (err) return err;
    });
    callback(1);
  });
};

exports.createOrder = (req, res, callback) => {
  const orders = req.body.orders;
  var total = 0;
  bookModel.getBooks(res, (result) => {

    const books = result.map((book) => {
      if (orders.indexOf(book.id) != -1) {
        return total += book.price;
      }
    });

    //save to db
    const query = "INSERT INTO orders SET ?";
    const data = {
      book_ids: JSON.stringify(orders),
      total_price: total,
      user_id: req.user.id,
      created_at: new Date()
    };

    connection.query(query, data, (err, rows, fields) => {
      if (err) return console.log(err);
      return 1;
    });

    return res.status(200).json({
      price: total
    });

  });
};