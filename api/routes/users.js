const userController = require('../controllers/user');
const auth = require('../middleware/auth');
const express = require('express')
const router = express.Router();

router.get('/me', auth, userController.getInfo);
router.post('/', userController.createUser);
router.post('/orders', auth, userController.createOrder);
router.delete('/', auth, userController.remove);

module.exports = router;