const bookController = require('../controllers/book');
const express = require('express')
const router = express.Router();

router.get('/', bookController.getBooks);

module.exports = router;