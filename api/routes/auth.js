const userController = require('../controllers/user');
const express = require('express')
const router = express.Router();

router.post('/', userController.login);

module.exports = router;