const fetch = require("node-fetch");

exports.getBooks = (req, res, next) => {
  const url = "https://scb-test-book-publisher.herokuapp.com/books";
  const getData = async url => {
    try {
      const response = await fetch(url);
      const data = await response.json();
      const recommend = await getRecommendBook();
      const recommendIds = recommend.map(book => book.id);

      let result = await sortBook(data);
      result = result.filter((book) => {
        const index = recommendIds.indexOf(book.id);
        if (index == -1) {
          book.is_recommended = false;
          return book;
        }
      });

      res.status(200).json(recommend.concat(result));
    } catch (error) {
      res.status(500).json(error);
    }
  };
  getData(url);
};


const getRecommendBook = async () => {
  const url = "https://scb-test-book-publisher.herokuapp.com/books/recommendation";
  try {
    const response = await fetch(url);
    const json = await response.json();
    const result = await sortBook(json);
    return result.map(book => {
      book.is_recommended = true
      return book;
    });
  } catch (error) {
    res.status(500).json(error);
  }
};

async function sortBook(data) {
  data.sort((curent, next) => {
    if (curent.book_name < next.book_name) {
      return -1;
    }
    if (curent.book_name > next.book_name) {
      return 1;
    }
    return 0;
  })
  return data;
}