require('dotenv').config();
const userModel = require('../models/user');
const jwt = require('jsonwebtoken');

exports.getInfo = (req, res, next) => {
  userModel.getInfo(req.user.id, (result) => {
    if (result.length == 0) return res.status(404).json('User not exists');
    res.status(200).json(result);
  });
}

exports.createUser = (req, res, next) => {
  userModel.create(req, res, (id) => {
    req.body.id = id;
    res.status(201).json('create new user successful');
  });
}

exports.login = (req, res, next) => {
  userModel.login(req, res, (response, user) => {
    if (response) {
      const data = {
        id: user.id
      };
      const token = jwt.sign(data, process.env.JWT_SECRET);
      res.status(200).json({
        token: token
      });
    } else {
      res.status(400).json('Invalid username or password!');
    }
  });
}

exports.remove = (req, res, next) => {
  userModel.delete(req.user.id, (result) => {
    res.status(200).json("Delete user successful");
  });
}

exports.createOrder = (req, res, next) => {
  userModel.createOrder(req, res, (result) => {
    res.status(200).json("Delete user successful");
  });
}