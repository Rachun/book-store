const userRoute = require('./api/routes/users');
const bookRoute = require('./api/routes/book');
const authRoute = require('./api/routes/auth');
const config = require('config');
const morgan = require('morgan');
const helmet = require('helmet');
const express = require('express');
const app = express();

app.use(helmet());
app.use(morgan('tiny'));
app.use(express.json());

app.use('/api/users', userRoute);
app.use('/api/books', bookRoute);
app.use('/api/login', authRoute);
app.use('/api/books', bookRoute);

app.post('/login', (req, res) => {
  res.send('login success');
});

app.use((req, res, next) => {
  const error = new Error('Not found!');
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message
    }
  });
});

module.exports = app;