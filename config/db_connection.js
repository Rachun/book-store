const mysql = require('mysql');
const config = require('config');

const connection = mysql.createConnection({
  host: config.get('db_host'),
  user: config.get('db_user'),
  password: config.get('db_password'),
  database: config.get('db_name')
});

connection.connect();

module.exports = connection;